import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses(){
	console.log(coursesData);

	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} course={course}/>
		)
	})
	return(
		<>
		{courses}
		</>
	)
}