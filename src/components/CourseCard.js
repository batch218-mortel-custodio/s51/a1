import {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

import PropTypes from 'prop-types';

export default function CourseCard({course}) {
  //console.log(props);
  //console.log(typeof props);

  const {name, description, price, id} = course;

  // const name = course.name; << same same

  //  setter , getter      , storage of state
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);

  function enroll() {
    // s55 ACTIVITY
    //if(seats === 1){}
    if (seats > 1){
      setSeats(seats - 1);
      setCount(count + 1);
      console.log('Available remaining seats: ' + seats);
      console.log('Enrollees: ' + count);
    }
    else{
      alert("No more seats left");
      setSeats(seats - 1);
      setCount(count + 1);
      document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
    }
  }





  return (
    <Card style={{ width: '100%' }}>

      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text>Available Seats: {seats}</Card.Text>
        <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
    </Card>
  );
}



CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}